﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KeyboxFileReader;

namespace BinToTrc
{
    public partial class FormKeyboxFileReader : Form
    {
        public FormKeyboxFileReader()
        {
            InitializeComponent();
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                if(dialog.ShowDialog() == DialogResult.OK)
                {
                    DateTime start = DateTime.Now;
                    textBoxFileName.Text = dialog.FileName;
                }
            }
            try
            {
                if (!String.IsNullOrEmpty(textBoxFileName.Text))
                {
                    BinReader Rdr = new BinReader();
                    int errors = Rdr.ReadKeyboxFile(textBoxFileName.Text);
                    MessageBox.Show("Total Errors on file " + errors.ToString());
                    textBoxResult.Text = Rdr.ToString();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show($"{ex.Message}\n {ex.StackTrace}");
            }
            
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxResult.Text == string.Empty)
                MessageBox.Show("nothing yet to save...");
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.DefaultExt = "trc";
                if(dialog.ShowDialog() == DialogResult.OK)
                {
                    File.AppendAllText(dialog.FileName, textBoxResult.Text);
                    MessageBox.Show($"Saved successfully to {dialog.FileName}");
                }
            }
        }
    }
}
