﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyboxFileReader
{
    public class BinReader
    {
        public int Odometer { get; set; }

        public string VIN { get; set; }

        public string BoudRate { get; set; }

        public int ErrorsCount { get; set; }

        public SeatBeltStatus SeatBeltStat { get; set; }

        public List<KeyboxRecord> Records { get; set; }

        public BinReader()
        {
            clear();
        }

        public int ReadKeyboxFile(string FileName, string ResultFile = null)
        {
            int ErrorSCount = -1;
            if (File.Exists(FileName))
            {
                byte[] FileData = File.ReadAllBytes(FileName);
                ErrorSCount = ResultFile == null ? DecodeData(FileData) : DecodeDataToFile(FileData, ResultFile);
            }
            this.ErrorsCount = ErrorSCount;
            return ErrorSCount;
        }

        public int FastRead(string FileName, out StringBuilder Result)
        {
            int ErrorSCount = -1;
            Result = new StringBuilder();
            if (File.Exists(FileName))
            {
                byte[] FileData = null;
                FileData = File.ReadAllBytes(FileName);
                ErrorSCount = DecodeDataFast(FileData, out Result);
            }
            return ErrorSCount;
        }

        public override string ToString()//TODO
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(BoudRate);
            sb.AppendLine(Odometer.ToString());
            foreach (KeyboxRecord record in Records)// maybe need to add a progress bar
                sb.AppendLine(record.ToString());
            sb.AppendLine(BoudRate);
            //need to add a value here !
            sb.AppendLine(VIN);
            sb.AppendLine(ErrorsCount.ToString());
        
            return sb.ToString();
        }

        private int DecodeData(byte[] FileData)
        {
            clear();
            int ErrorsCount = 0;
            long index = 0;
            long lc = FileData.LongCount();
            while (index + 24 < lc)
            {
                KeyboxRecord Rcrd = buildRecord(FileData, ref index, lc);
                ErrorsCount += addRecord(Rcrd);
            }
            return ErrorsCount;
        }

        private int DecodeDataToFile(byte[] FileData, string path)
        {
            clear();
            int ErrorsCount = 0;
            long index = 0;
            long lc = FileData.LongCount();

            while (index + 24 < lc)
            {
                KeyboxRecord Rcrd = buildRecord(FileData, ref index, lc);
                ErrorsCount += addRecord(Rcrd);
                File.AppendAllText(path, string.Format("{0}{1}", Rcrd.ToString(), Environment.NewLine));
            }
            return ErrorsCount;
        }

        private int DecodeDataFast(byte[] FileData, out StringBuilder Result)
        {
            clear();
            Result = new StringBuilder();
            int ErrorsCount = 0;
            long index = 0;
            long lc = FileData.LongCount();
            while (index + 24 < lc)
            {
                KeyboxRecord Rcrd = buildRecord(FileData, ref index, lc);
                ErrorsCount += addRecord(Rcrd);
                Result.Append(string.Format("{0}{1}", Rcrd.ToString(), Environment.NewLine));
            }
            return ErrorsCount;
        }

        private KeyboxRecord buildRecord(byte[] FileData, ref long index, long lc)
        {
            KeyboxRecord Rcrd = null;
            if (FileData[index + 23] == 0x0a)
            {
                byte[] rex = new byte[24];
                for (int i = 0; i < 24; i++)
                    rex[i] = FileData[index + i];
                Rcrd = new KeyboxRecord(rex, SeatBeltStat);
                if (!Rcrd.DataError)
                    index += 24;
                else
                {
                    index++;
                    while (index < lc && FileData[index] != 0x0a)
                        index++;
                }
            }
            else
            {
                List<byte> rex = new List<byte>();
                while (index < lc && FileData[index] != 0x0a && rex.Count < 24)// did not check size of rex before
                {
                    rex.Add(FileData[index]);
                    index++;
                }
                if (index < lc) 
                    rex.Add(FileData[index]);
                Rcrd = new KeyboxRecord(rex.ToArray(), SeatBeltStat);
                index++;
            }

            return Rcrd;
        }

        private int addRecord(KeyboxRecord Rcrd)
        {
            bool hasError = true;
            if (Rcrd != null)
            {
                hasError = false;
                Records.Add(Rcrd);
                switch (Rcrd.RecType)
                {
                    case RecordType.Invalid:
                        hasError = true;
                        break;
                    case RecordType.Odometer:
                        if (Rcrd.Odometer > 100 || Odometer == 0) 
                            Odometer = Rcrd.Odometer;
                        else 
                            Rcrd.Odometer = (Rcrd.Odometer % 100) + ((Odometer / 100) * 100);
                        break;
                    case RecordType.VIN:
                        VIN = Rcrd.VIN;
                        break;
                    case RecordType.BoudRate:
                        if (Rcrd.BoudRate != "")
                            BoudRate = Rcrd.BoudRate;
                        break;
                    case RecordType.SeatBelt:
                        SeatBeltStat = Rcrd.SB_Status;
                        break;
                    default:
                        break;
                }
            }

            return hasError ? 1 : 0;   
        }

        private void clear()
        {
            Odometer = 0;
            ErrorsCount = 0;
            VIN = string.Empty;
            BoudRate = string.Empty;
            Records = new List<KeyboxRecord>();
            SeatBeltStat = SeatBeltStatus.Unknown;
        }
    }
}
