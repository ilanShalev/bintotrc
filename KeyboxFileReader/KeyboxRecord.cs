﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyboxFileReader
{
    public enum RecordType { Regular, Odometer, Invalid, VIN, BoudRate, WriteError, SeatBelt }
    public enum SeatBeltStatus { Unknown, buckledStart, buckled, unbuckledStart, unbuckled }
    public class KeyboxRecord
    {
        internal byte[] OriginalRecord { get; set; }
        internal int OriginalRecordLenght { get; set; }
        internal long MsgNo { get; set; }
        internal ulong MsgID { get; set; }
        internal double Time { get; set; }
        internal int Length { get; set; }
        internal int DataLength { get; set; }
        internal byte[] Data { get; set; }
        internal string RxTx { get; set; }
        internal RecordType RecType { get; set; }
        internal int Odometer { get; set; }
        internal string VIN { get; set; }
        internal string BoudRate { get; set; }
        internal bool DataError { get; set; }
        internal SeatBeltStatus SB_Status { get; set; }

        internal KeyboxRecord()
        {
            Clear();
        }

        internal KeyboxRecord(byte[] InData, SeatBeltStatus status)
        {
            Clear();
            //InData.CopyTo(OriginalRecord, 0);
            OriginalRecord = InData;
            OriginalRecordLenght = OriginalRecord.Count();
            DataError = false;
            setRecData(InData, status);
        }

        internal void Clear()
        {
            OriginalRecord = null;
            OriginalRecordLenght = 0;
            MsgNo = 0;
            MsgID = 0;
            Time = 0.0;
            Length = 0;
            DataLength = 0;
            Data = null;
            RxTx = "  ";
            RecType = RecordType.Invalid;
            Odometer = 0;
            VIN = "";
            BoudRate = "";
            DataError = false;
            SB_Status = SeatBeltStatus.Unknown;
        }

        internal string Decode(byte[] InData, SeatBeltStatus stat)
        {
            Clear();
            //if (InData.Count() > 22 && InData[0] == 0xFF && InData[1] == 0xFF &&
            //                          InData[2] == 0xFF && InData[3] == 0xFF)
            //RecType = KbrRecordType.Odometer;
            OriginalRecord = InData;
            OriginalRecordLenght = OriginalRecord.Count();
            setRecData(InData, stat);

            return this.ToString();
        }

        public override string ToString()
        {
            //1049572)    408313.2  Rx         0287  8  00 00 00 00 00 00 00 00
            string Resp = "";
            switch (RecType)
            {
                case RecordType.Regular:
                    Resp = MsgNo.ToString("D7") + ")";
                    string tmpStr = Time.ToString("0.0");
                    while (tmpStr.Length < 12) { tmpStr = " " + tmpStr; }
                    Resp = Resp + tmpStr;
                    Resp = Resp + "  " + RxTx;
                    if (MsgID <= 0xFFFF)
                    {
                        tmpStr = MsgID.ToString("X4");
                    }
                    else
                    {
                        tmpStr = MsgID.ToString("X8");
                    }
                    while (tmpStr.Length < 13) { tmpStr = " " + tmpStr; }
                    Resp = Resp + tmpStr;
                    Resp = Resp + "  " + DataLength.ToString() + " ";
                    for (int i = 0; i < Data.Count(); i++) { Resp = Resp + " " + Data[i].ToString("X2"); }
                    Resp = Resp + " ";
                    if (MsgNo == 7885312)
                    {
                        Resp = "; 7885312 Data Error ";
                        for (int i = 0; i < OriginalRecordLenght; i++) { Resp = Resp + " " + OriginalRecord[i].ToString("X2"); }
                        break;
                    }
                    else if (DataLength == 0 || MsgNo >= 10000000 || MsgNo < 0)
                    {
                        Resp = "; Msg Id Data Error ";
                        for (int i = 0; i < OriginalRecordLenght; i++) { Resp = Resp + " " + OriginalRecord[i].ToString("X2"); }
                        break;
                    }
                    else if (DataError)
                    {
                        Resp = "; Data Error ";
                        for (int i = 0; i < OriginalRecordLenght; i++) { Resp = Resp + " " + OriginalRecord[i].ToString("X2"); }
                    }
                    break;
                case RecordType.Odometer:
                    //Resp = "; Odometer =" + Odometer.ToString() + " Time= " + Time.ToString("0.0");
                    Resp = ";LinearValue;Time=" + Time.ToString("0.0") + ";Val=" + Odometer.ToString() + ";";
                    break;
                case RecordType.VIN:
                    Resp = "; VIN=" + VIN;
                    break;
                case RecordType.BoudRate:
                    Resp = "; BoudRate=" + BoudRate;
                    break;
                case RecordType.WriteError:
                    Resp = "; Write Error ";
                    for (int i = 0; i < OriginalRecordLenght; i++) { Resp = Resp + " " + OriginalRecord[i].ToString("X2"); }
                    break;
                case RecordType.SeatBelt:
                    Resp = ";BooleanValue;Time=" + Time.ToString("0.0") + ";SeatBelt=";
                    switch (SB_Status)
                    {
                        case SeatBeltStatus.buckled:
                            Resp = Resp + "ON";
                            break;
                        case SeatBeltStatus.unbuckled:
                            Resp = Resp + "OFF";
                            break;
                        default:
                            Resp = Resp + "Undefined";
                            break;
                    }
                    break;
                default:
                    Resp = "; Data Error ";
                    for (int i = 0; i < OriginalRecordLenght; i++)
                    {
                        Resp = Resp + " " + OriginalRecord[i].ToString("X2");
                    }
                    break;
            }
            return Resp;
        }

        private void setRecData(byte[] InData, SeatBeltStatus stat)
        {
            if (InData.Count() > 20 && InData[0] == 0xFF && InData[1] == 0xFF &&
                                        InData[2] == 0xFF && InData[3] == 0xFF)
            {
                RecType = RecordType.Odometer;
                setOdometerType();
            }
            else if (InData.Count() > 23 && InData[0] == 0xDD && InData[1] == 0xDD &&
                                        InData[2] == 0xDD && InData[3] == 0xDD)
            {
                RecType = RecordType.BoudRate;
                setBoudRateType();
            }
            else if (InData.Count() > 23 && InData[0] == 0xEE && InData[1] == 0xEE &&
                                        InData[2] == 0xEE && InData[3] == 0xEE)
            {
                RecType = RecordType.VIN;
                setVinType();
            }
            else if (InData.Count() > 23 && InData[0] == 0xBB && InData[1] == 0xBB &&
                                        InData[2] == 0xBB && InData[3] == 0xBB)
            {
                RecType = RecordType.SeatBelt;
                setSeatBeltType(stat);
            }
            else if (InData.Count() > 23 && InData[0] == 0xAA && InData[1] == 0xAA &&
                                        InData[2] == 0xAA && InData[3] == 0xAA)
            {
                RecType = RecordType.WriteError;
                setWriteErrorType();
            }
            else if ((InData.Count() == 24) && InData[23] == 0x0A && InData[14] <= 8)
            {
                RecType = RecordType.Regular;
                setRegularType();
            }
            else
                RecType = RecordType.Invalid;
        }

        private void setWriteErrorType()//TODO
        {
            throw new NotImplementedException();
        }

        private void setSeatBeltType(SeatBeltStatus stat)
        {
            Time = OriginalRecord[4] + OriginalRecord[5] * 0x100 + OriginalRecord[6] * 0x10000 + OriginalRecord[7] * 0x1000000;
            switch (OriginalRecord[9])
            {
                case 0x1A:
                    if (stat != SeatBeltStatus.buckledStart && stat != SeatBeltStatus.buckled)
                        SB_Status = SeatBeltStatus.buckledStart;
                    else SB_Status = SeatBeltStatus.buckled;
                    break;
                case 0x19:
                    if (stat != SeatBeltStatus.unbuckledStart && stat != SeatBeltStatus.unbuckled)
                        SB_Status = SeatBeltStatus.unbuckledStart;
                    else SB_Status = SeatBeltStatus.unbuckled;
                    break;
                default:
                    SB_Status = SeatBeltStatus.Unknown;
                    break;
            }
        }

        private void setVinType()
        {
            VIN = "";
            for (int i = 0; i < 17; i++)
            {
                VIN = VIN + (char)(OriginalRecord[4 + i]);
            }
        }

        private void setBoudRateType()
        {
            switch (int.Parse(OriginalRecord[5].ToString("X") + OriginalRecord[4].ToString("X"), 
                System.Globalization.NumberStyles.HexNumber)) //was % 5
            {
                case 5:
                    BoudRate = "5 KBit/s";
                    break;
                case 10:
                    BoudRate = "10 KBit/s";
                    break;
                case 20:
                    BoudRate = "20 KBit/s";
                    break;
                case 33:
                    BoudRate = "33 KBit/s";
                    break;
                case 47:
                    BoudRate = "47 KBit/s";
                    break;
                case 50:
                    BoudRate = "50 KBit/s";
                    break;
                case 83:
                    BoudRate = "83 KBit/s";
                    break;
                case 95:
                    BoudRate = "95 KBit/s";
                    break;
                case 100:
                    BoudRate = "100 KBit/s";
                    break;
                case 125:
                    BoudRate = "125 KBit/s";
                    break;
                case 250:
                    BoudRate = "250 KBit/s";
                    break;
                case 500:
                    BoudRate = "500 KBit/s";
                    break;
                case 800:
                    BoudRate = "800 KBit/s";
                    break;
                case 1000:
                    BoudRate = "1000 KBit/s";
                    break;
                default:
                    BoudRate = "";
                    break;
            }
        }

        private void setOdometerType()
        {
            Time = OriginalRecord[4] + OriginalRecord[5] * 0x100 + OriginalRecord[6] * 0x10000 + OriginalRecord[7] * 0x1000000;
            //string odo = OriginalRecord[13].ToString() + (char)OriginalRecord[12].ToString() + (char)OriginalRecord[11] + (char)OriginalRecord[10]
            //  (char)OriginalRecord[9] + (char)OriginalRecord[8];
            Odometer = 0;
            int tmpint = 0;
            for (int i = 0; i < 5; i++)
            {
                tmpint = OriginalRecord[8 + i] >= 0x30 && OriginalRecord[8 + i] < 0x3A ? OriginalRecord[8 + i] - 48 : 0;
                //Odometer = Odometer + (int)(tmpint * Math.Pow(10, i));
                if (OriginalRecord[8 + i] != 0x00) Odometer = Odometer * 10 + tmpint;
            }
        }

        private void setRegularType()
        {
            MsgNo = OriginalRecord[0] + OriginalRecord[1] * 0x100 + OriginalRecord[2] * 0x10000 + OriginalRecord[3] * 0x1000000;
            Time = OriginalRecord[4] + OriginalRecord[5] * 0x100 + OriginalRecord[6] * 0x10000 + OriginalRecord[7] * 0x1000000;
            switch ((char)OriginalRecord[8])
            {
                case 'R':
                    RxTx = "Rx";
                    break;
                case 'T':
                    RxTx = "Tx";
                    break;
                default:
                    RxTx = "  ";
                    break;
            }
            MsgID = (ulong)OriginalRecord[10] + (ulong)OriginalRecord[11] * 0x100 + (ulong)OriginalRecord[12] * 0x10000 + (ulong)OriginalRecord[13] * 0x1000000;
            DataLength = OriginalRecord[14];
            Data = new byte[DataLength];
            if (DataLength <= 8) for (int i = 0; i < DataLength; i++) Data[i] = OriginalRecord[15 + i];
            if (MsgNo == 7885312 || DataLength == 0 || MsgNo >= 10000000 || MsgNo < 0 || Time < 0 || Time > 1000000000) DataError = true;
        }
    }
}
